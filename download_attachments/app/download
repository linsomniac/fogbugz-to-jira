#!/usr/bin/env python

from selenium import webdriver
import re
import urllib
import requests
import glob
import os
import random

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-setuid-sandbox')   # @@@ Recently added per https://qxf2.com/blog/chrome-not-reachable-error/
chrome_options.add_argument('--disable-background-networking')   # @@@ Recently added per https://qxf2.com/blog/chrome-not-reachable-error/
chrome_options.add_argument('--window-size=1420,1080')
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get(os.environ['BUGZ_URL'] + 'login')
driver.find_element_by_id('sPerson').send_keys(os.environ['BUGZ_USERNAME'])
driver.find_element_by_id('sPassword').send_keys(os.environ['BUGZ_PASSWORD'])
driver.find_element_by_xpath("//input[@value='Log In']").click()

def download_attachment(bug, attachment):
    newFileName = '/attachments/{}.data'.format(attachment['ixAttachment'])
    if os.path.exists(newFileName):
        return

    url = os.environ['BUGZ_URL'] + attachment['sURL'].decode()
    url = re.sub(r'&amp;', '&', url)

    print('Download attachment {}/{}: {} {}'.format(
        bug['ixBug'],
        attachment['ixAttachment'],
        attachment['sFileName'],
        url))

    driver.get(url)

    page_source = driver.page_source

    m = re.search(r'src="(https://files.manuscript.com/[^"]+)"', page_source)
    if not m:
        print(
            'Unable to find files.manuscript URL for bug %s in attachment %s in: %s'
            % (bug['ixBug'], attachment['ixAttachment'], page_source))
        return
    url = m.group(1)
    url = re.sub(r'&amp;', '&', url)

    r = requests.get(url, allow_redirects=True)
    with open(newFileName, 'wb') as fp:
        fp.write(r.content)


def process_bug(filename):
    try:
        bug = eval(open(filename, 'r').read())
    except SyntaxError:
        return

    for event in bug['events']:
        if event['rgAttachments'] is None:
            continue
        for attachment in event['rgAttachments']:
            if attachment is None:
                continue
            download_attachment(bug, attachment)

l = glob.glob('/bugz-data/*.data')
for filename in l:
    process_bug(filename)
