My organization is going through migrating from FogBugz by FogCreek to Jira
and Confluence by Atlassian.  This repo contains a series of tools which we
are using to migrate our content.

Currently, Dec 2019, I'm starting to commit the code, but I can't just copy it
over from our systems.  I'm having to go through and do some clean up and
removal of account names and passwords.

Setup
=====

The following environment variables are used for configurable elements:

    ENV BUGZ_URL https://example.manuscript.com/
    ENV BUGZ_USERNAME user@exaple.com
    ENV BUGZ_PASSWORD xyzzy

Downloading Attachments
=======================

The `"download_attachments"` directory contains a Docker container that uses
Selenium to download the attachments to cases.

The code currently expects FogBugz cases to have been downloaded into pickled
files under "bugz-data".  The code that does this is the next piece of code I
plan to clean up and release.

Also, this expects the attachments to be in a field named "rgAttachments".
Edit the code if your attachments are named differently.

Build with:

    cd download_attachments
    sudo docker build --name download_attachments .

And then run with:

    sudo docker run --shm-size=5g --rm \
        --mount type=bind,source=$(pwd)/app,destination=/app \
	--mount type=bind,source=$(pwd)/attachments,destination=/attachments \
	--mount type=bind,source=$(pwd)/bugz-data,destination=/bugz-data \
	--name download_attachments -it download_attachments
